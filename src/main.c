#include "test.h"
#include <inttypes.h>
#include <stdio.h>

int main() {
	fprintf(stdout, "Enter the number of the test you want: \n"
	"1) Common memory allocation. \n"
	"2) free one block from several allocated. \n"
	"3) free two blocks from several allocated. \n"
	"4) expansion of the old regiob of memory with a new, in case old is full. \n"
	"5) allocation of new memory region in a different location, when the old one can't be expanded due to another allocated range of adresses. \n");
	
	
	uint8_t in_v;
	scanf("%" SCNu8, &in_v);
	
	switch (in_v) {
		case 1:
			test_one();
			break;
		case 2:
			test_two();
			break;
		case 3:
			test_three();
			break;
		case 4:
			test_four();
			break;
		case 5:
			test_five();
			break;
		default:
			fprintf(stderr, "Enter a number from 1 to 5. \n");
			break;
	}
	
	return 0;

}
