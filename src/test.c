#include "test.h"
#include "mem.h"

void test_one() {
	void* heap = heap_init(500);
	debug_heap(stdout, heap);
	_malloc(1);
	_malloc(400);
	debug_heap(stdout, heap);
}

void test_two() {
	void* heap = heap_init(500);
	void *ptr = _malloc(200);
	_malloc(200);
	debug_heap(stdout, heap);
	_free(ptr);
	debug_heap(stdout, heap);
}

void test_three() {
	void* heap = heap_init(500);
	void* ptr_one = _malloc(100);
	_malloc(100);
	void* ptr_two = _malloc(100);
	debug_heap(stdout, heap);
	_free(ptr_one);
	_free(ptr_two);
	debug_heap(stdout, heap);
}

void test_four() {
	void* heap = heap_init(10000);
	_malloc(5000);
	debug_heap(stdout, heap);
	_malloc(10000);
	debug_heap(stdout, heap);
}

static void* map_pages_c(void const* addr, size_t length, int additional_flags) {
	return mmap((void*) addr, length, PROT_READ | PROT_WRITE, additional_flags, 0, 0);
}

void test_five() {
	void* heap = heap_init(8096);
	_malloc(100);
	debug_heap(stdout, heap);
	map_pages_c(heap, 10000, 0);
	_malloc(10000);
	debug_heap(stdout, heap);
}
	
	
	
	
	
	
	
	
	
	
	
	
	
